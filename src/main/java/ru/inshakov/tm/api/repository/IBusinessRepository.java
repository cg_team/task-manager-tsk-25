package ru.inshakov.tm.api.repository;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IRepository;
import ru.inshakov.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

    void clear(@NotNull String userId);

    @NotNull
    List<E> findAll(@NotNull String userId);

    @Nullable
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator);

    @Nullable
    E findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    E findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    E findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    E removeOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    E removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    E removeOneByName(@NotNull String userId, @NotNull String name);

}
