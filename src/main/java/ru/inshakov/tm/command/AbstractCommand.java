package ru.inshakov.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.service.IServiceLocator;
import ru.inshakov.tm.enumerated.Role;

@NoArgsConstructor
public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public void setServiceLocator(final @NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    public abstract String arg();

    @NotNull
    public abstract String name();

    @Nullable
    public abstract String description();

    public abstract void execute();

    @Nullable
    public Role[] roles() {
        return null;
    }

}
