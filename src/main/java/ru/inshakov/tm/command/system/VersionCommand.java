package ru.inshakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.command.AbstractCommand;

public final class VersionCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String description() {
        return "App version";
    }

    @Override
    public void execute() {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        System.out.println("[VERSION]");
        System.out.println(propertyService.getApplicationVersion());
    }

}
