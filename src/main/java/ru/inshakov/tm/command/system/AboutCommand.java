package ru.inshakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "Developer info";
    }

    @Override
    public void execute() {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        System.out.println("[ABOUT]");
        System.out.println("AUTHOR:" + propertyService.getAuthor());
        System.out.println("E-MAIL:" + propertyService.getAuthorEmail());
    }

}
